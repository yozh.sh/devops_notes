#!/bin/bash

RELEASE=release/latest/
DIST=xUbuntu_$(lsb_release -sr)
URL=http://download.bareos.org/bareos/$RELEASE/$DIST 
printf "deb $URL /\n" | sudo tee  /etc/apt/sources.list.d/bareos.list

wget -q $URL/Release.key -O- | sudo apt-key add -

sudo apt update

sudo apt install bareos-filedaemon
